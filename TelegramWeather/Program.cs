﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using HtmlAgilityPack;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;

namespace TelegramWeather
{
    class Program
    {
        private static readonly TelegramBotClient Bot = new TelegramBotClient("243615127:AAHXV9i_S6DGVJM87f33RXLAz_xFyl5P9z8");

        static void Main(string[] args)
        {
            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            var me = Bot.GetMeAsync().Result;

            Console.Title = me.Username;

            Bot.StartReceiving();
            Console.ReadLine();
            Bot.StopReceiving();
        }

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Debugger.Break();
        }

        private static void BotOnChosenInlineResultReceived(object sender,
            ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            Console.WriteLine(
                $"Received choosen inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }

        public static string GetResponse(string uri)
        {
            StringBuilder sb = new StringBuilder();
            byte[] buf = new byte[8192];
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();
            int count = 0;
            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    sb.Append(Encoding.Default.GetString(buf, 0, count));
                }
            } while (count > 0);
            return sb.ToString();
        }

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if (message == null || message.Type != MessageType.TextMessage) return;

            if (message.Text.StartsWith("/weather"))
            {
                var html = GetResponse("http://pogoda.by/33526"); // load HTML weather-page

                HtmlDocument htmlSnippet = new HtmlDocument();
                htmlSnippet.LoadHtml(html);

                List<string> legendList = new List<string>();
                List<List<string>> weatherValues = new List<List<string>>();

                var weatherBlock = htmlSnippet.DocumentNode.SelectSingleNode("//div[contains(@class, 'middle')]"); // find all weather block
                htmlSnippet.LoadHtml(weatherBlock.InnerHtml);
                var tableWeatherBlock = htmlSnippet.DocumentNode.SelectSingleNode("//table"); 
                foreach (var item in tableWeatherBlock.SelectNodes("//tr"))
                {
                    HtmlDocument htmlSnippetTable = new HtmlDocument();
                    htmlSnippetTable.LoadHtml(item.InnerHtml);
                    legendList.Add(htmlSnippetTable.DocumentNode.SelectSingleNode("//td[contains(@class, 'legend')]").InnerText); // find and add legend of values

                    List<string> listValuesOfWeather = new List<string>(); // temp list for values of weather

                    var dateValue = htmlSnippetTable.DocumentNode.SelectNodes("//td[contains(@class, 'dat')]"); 
                    if (dateValue != null) 
                    {
                        foreach (var innerItem in dateValue) // only for date exuc
                        {
                            listValuesOfWeather.Add(innerItem.InnerText);
                        }
                    }

                    HtmlDocument htmlSnippet2 = new HtmlDocument();
                    var weatherTableValue = htmlSnippetTable.DocumentNode.SelectNodes("//td[contains(@class, 'meteo')]");
                    if (weatherTableValue != null)
                    {
                        foreach (var innerItem in weatherTableValue) // other value exuc
                        {
                            htmlSnippet2.LoadHtml(innerItem.InnerHtml);
                            if (innerItem.InnerText.Equals("\n ")) // only get title text for image
                            {
                                listValuesOfWeather.Add(htmlSnippet2.DocumentNode.SelectSingleNode("//img").GetAttributeValue("title", ""));
                            }
                            else
                            {
                                listValuesOfWeather.Add(innerItem.InnerText);
                            }
                        }
                    }
                    weatherValues.Add(listValuesOfWeather);
                }

                //create message list
                List<string> messages = new List<string>();
                for (int i = 0; i < weatherValues[0].Count; i++)
                {
                    string tempMessage = "";
                    for (int j = 0; j < legendList.Count; j++)
                    {
                        if (j == 0)
                        {
                            tempMessage += "Время: " + ".......... " + weatherValues[j][i] + "\n";
                        }
                        else
                            tempMessage += legendList[j] + ": .......... " + weatherValues[j][i] + "\n";
                    }
                    messages.Add(tempMessage);
                }
                //send weather
                await Bot.SendTextMessageAsync(message.Chat.Id, "Погода в Ивано Франковске на ближайшее время:");
                foreach (var item in messages)
                {
                    await Bot.SendTextMessageAsync(message.Chat.Id, item + "\n=====================\n");
                    Thread.Sleep(2000); // wait 2 sec becouse too many request
                }
                Console.WriteLine("Send weather to " + messageEventArgs.Message.From.FirstName + " " + messageEventArgs.Message.From.LastName);
            }
            else
            {
                await Bot.SendTextMessageAsync(message.Chat.Id, "Type command /weather");
            }
        }
    }
}
